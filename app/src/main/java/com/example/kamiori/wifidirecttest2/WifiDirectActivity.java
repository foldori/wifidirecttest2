package com.example.kamiori.wifidirecttest2;

import android.content.Context;
import android.content.IntentFilter;
import android.graphics.Color;
import android.net.wifi.WpsInfo;
import android.net.wifi.p2p.WifiP2pConfig;
import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pDeviceList;
import android.net.wifi.p2p.WifiP2pInfo;
import android.net.wifi.p2p.WifiP2pManager;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class WifiDirectActivity extends AppCompatActivity {
    public static final String TAG = "WifiDirectActivity";
    public static final String MODE_GROUP_OWNER = "server";
    public static final String MODE_CLIENT = "client";

    private WifiP2pManager manager;
    private WifiP2pManager.Channel channel;

    private IntentFilter intentFilter = new IntentFilter();
    private WifiDirectBroadcastReceiver receiver;


    private String mode = MODE_CLIENT;

    private boolean isConnected = false;

    private ServerSocket serverSocket = null;
/*
    //一定時間ほど待ち、他の端末の有無によりサーバかクライアントかを判断する
    private Handler activateServerHandler = new Handler();
    private Runnable activateServerTask = new Runnable() {
        @Override
        public void run() {
            boolean clientFlag = deviceList != null && deviceList.getDeviceList().size() > 0;
            clientFlag = false;
            if(clientFlag){
                //他のデバイスが見つかった
                Log.d(WifiDirectActivity.TAG, "Client mode");
                receiver.setMode(MODE_CLIENT);
            }else{
                //見つからなかった
                Log.d(WifiDirectActivity.TAG, "Server mode");
                receiver.setMode(MODE_GROUP_OWNER);
                mode = MODE_GROUP_OWNER;
            }
        }
    };
*/

    //一定時間ごとにデバイス一覧を取得するイベント
    private Timer requestPeersTimer = new Timer();
    private TimerTask requestPeersTask = new TimerTask() {
        @Override
        public void run() {
            if(isConnected) {
                manager.discoverPeers(channel, new WifiP2pManager.ActionListener() {
                    @Override
                    public void onSuccess() {
//                    Toast.makeText(getApplicationContext(), "Discovery Initiated", Toast.LENGTH_SHORT).show();
//                    Log.d(WifiDirectActivity.TAG, "Discovery Initiated");
                    }

                    @Override
                    public void onFailure(int reason) {
//                    Toast.makeText(getApplicationContext(), "Discovery Failed : " + reason, Toast.LENGTH_SHORT).show();
//                    Log.d(WifiDirectActivity.TAG, "Failed : " + reason);
                    }
                });
            }
        }
    };

    //メッセージ一覧を入れる
    public ArrayAdapter<String> messageAdapter;

    //接続先のMACアドレスとソケットを保持するマップ
    private List<Socket> socketList;
    private HashSet<String> deviceNameList;

    public boolean isConnected() {
        return isConnected;
    }
    public void setConnected(boolean connected) {
        isConnected = connected;
    }

    public String getMode() {
        return mode;
    }
    public void setMode(String mode) {
        this.mode = mode;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wifi_direct);

        intentFilter.addAction(WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION);
        intentFilter.addAction(WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION);
        intentFilter.addAction(WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION);
        intentFilter.addAction(WifiP2pManager.WIFI_P2P_DISCOVERY_CHANGED_ACTION);
        intentFilter.addAction(WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION);

        manager = (WifiP2pManager) getSystemService(Context.WIFI_P2P_SERVICE);
        channel = manager.initialize(this, getMainLooper(), null);

        socketList = Collections.synchronizedList(new ArrayList<Socket>());
        deviceNameList = new HashSet<>();

        messageAdapter = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_list_item_1){
            public View getView(int position, View convertView, ViewGroup parent){
                View view = super.getView(position, convertView, parent);
                TextView textView = (TextView) view.findViewById(android.R.id.text1);
                textView.setTextColor(Color.BLUE);

                return view;
            }
        };
        ListView listView = (ListView) findViewById(R.id.list_view);
        listView.setAdapter(messageAdapter);

        //サーバ/クライアントの判断を予約
//        activateServerHandler.postDelayed(activateServerTask, 10000);

        //接続可能なデバイスの探索を開始
        requestPeersTimer.scheduleAtFixedRate(requestPeersTask, 0, 500);

        ((Button) findViewById(R.id.button_connect)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                manager.discoverPeers(channel, new WifiP2pManager.ActionListener() {

                    @Override
                    public void onSuccess() {
                        Toast.makeText(getApplicationContext(), "Discovery Initiated", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onFailure(int reason) {
                        Toast.makeText(getApplicationContext(), "Discovery Failed : " + reason, Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });

        Button button = (Button) findViewById(R.id.button_send);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(WifiDirectActivity.TAG, "Clicked");
                EditText editText = (EditText) findViewById(R.id.edit_text);
                new SendAsyncTask().executeOnExecutor(
                        AsyncTask.THREAD_POOL_EXECUTOR, editText.getText().toString());
            }
        });
    }

    //接続可能なデバイスの中にGOが存在するか調べる
    public void searchGroupOwner(WifiP2pDeviceList deviceList){
        if(isConnected)return;

        WifiP2pDevice defaultDevice = null;
        for(WifiP2pDevice device : deviceList.getDeviceList()){
            if(defaultDevice == null){
                defaultDevice = device;
            }

            if(device.isGroupOwner()){
                if(device.status != 3){
                    if(defaultDevice.status != 3){
                        Log.d(WifiDirectActivity.TAG, "default device is not available");
                    }
                    return;
                }
                //見つかった場合、はじめに見つかったものに対して接続する
                Log.d(WifiDirectActivity.TAG, "Connecting : \n" + device.toString());
                WifiP2pConfig config = new WifiP2pConfig();

                config.deviceAddress = device.deviceAddress;
                config.wps.setup = WpsInfo.PBC;
                config.groupOwnerIntent = 15;

                manager.connect(channel, config, new WifiP2pManager.ActionListener() {
                    @Override
                    public void onSuccess() {
//                        Toast.makeText(getApplicationContext(), "Connection initiated", Toast.LENGTH_SHORT).show();
                        Log.d(WifiDirectActivity.TAG, "Connection initiated");
                    }

                    @Override
                    public void onFailure(int reason) {
//                        Toast.makeText(getApplicationContext(), "Connection failed", Toast.LENGTH_SHORT).show();
                        Log.d(WifiDirectActivity.TAG, "Connection failed : " + reason);
                    }
                });
                return;
            }else{

            }
        }
        if(defaultDevice == null){
            Log.d(WifiDirectActivity.TAG, "default device is null");
            return;
        }
        Log.d(WifiDirectActivity.TAG, "Default device : \n" + defaultDevice.toString());
        if(defaultDevice.status != 3){
            Log.d(WifiDirectActivity.TAG, "default device is not available");
            return;
        }

        Log.d(WifiDirectActivity.TAG, "Connecting : \n" + defaultDevice.toString());
        WifiP2pConfig config = new WifiP2pConfig();

        config.deviceAddress = defaultDevice.deviceAddress;
        config.wps.setup = WpsInfo.PBC;
        config.groupOwnerIntent = 15;

        manager.connect(channel, config, new WifiP2pManager.ActionListener() {
            @Override
            public void onSuccess() {
//                Toast.makeText(getApplicationContext(), "Connection succeeded", Toast.LENGTH_SHORT).show();
                Log.d(WifiDirectActivity.TAG, "Connection succeeded");
            }

            @Override
            public void onFailure(int reason) {
//                Toast.makeText(getApplicationContext(), "Connection failed", Toast.LENGTH_SHORT).show();
                Log.d(WifiDirectActivity.TAG, "Connection failed : " + reason);
            }
        });

    }
    public void notifyConnection(WifiP2pInfo info) {
        if(info.isGroupOwner){
            mode = MODE_GROUP_OWNER;
            debug("Group Owner");
        }else{
            debug("Client");
        }
        new ReceiveAsyncTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, info);

    }

    @Override
    public void onResume() {
        super.onResume();

        receiver = new WifiDirectBroadcastReceiver(manager, channel, this);
        registerReceiver(receiver, intentFilter);
    }

    @Override
    public void onPause(){
        super.onPause();
        unregisterReceiver(receiver);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            for(Socket socket : socketList){
                socket.close();
            }
        } catch (IOException e) {
            Log.e(WifiDirectActivity.TAG, e.toString());
        }
        manager.removeGroup(channel, null);
    }

    class SendAsyncTask extends AsyncTask<String, String, String>{
        @Override
        protected String doInBackground(String... params) {
            try {
                if (mode.equals(MODE_CLIENT)) {
                    Log.d(WifiDirectActivity.TAG, "Send : client mode");
                    Log.d(WifiDirectActivity.TAG, "socket : " + Arrays.toString(socketList.toArray(new Socket[]{})));
                    //クライアント時
                    for(Socket socket : socketList){
                        BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
                        Log.d(WifiDirectActivity.TAG, "Sent : " + params[0]);
                        writer.write(params[0]+"\n");
                        writer.flush();
                    }
                } else if(mode.equals(MODE_GROUP_OWNER)){
                    Log.d(WifiDirectActivity.TAG, "Send : server mode");
                    Log.d(WifiDirectActivity.TAG, "socket : " + Arrays.toString(socketList.toArray(new Socket[]{})));
                    //サーバ時
                    for(Socket socket : socketList){
                        BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
                        Log.d(WifiDirectActivity.TAG, "Sent : " + params[0]);
                        writer.write(params[0]+"\n");
                        writer.flush();
                    }
                }
                Log.d(WifiDirectActivity.TAG, "Sending finished");
            }catch(Exception e){
                Log.e(WifiDirectActivity.TAG, e.toString());
            }
            return null;
        }
    }

    class ReceiveAsyncTask extends AsyncTask<WifiP2pInfo, String, Void>{
        @Override
        protected Void doInBackground(WifiP2pInfo... params) {
            WifiP2pInfo info = params[0];
            Socket socket = null;
            if(mode.equals(MODE_CLIENT)){
                socket = new Socket();
                Log.d(WifiDirectActivity.TAG, "Create socket : client mode");

                try {
                    socket.bind(null);
                } catch (IOException e) {
                    Log.e("ClientMode/Socket1", e.toString());
                }

                try {
                    socket.connect(new InetSocketAddress(info.groupOwnerAddress.getHostAddress(), 8988), 5000);
                } catch (IOException e) {
                    Log.e("ClientMode/Socket2", e.toString());
                }

                socketList.add(socket);
                Log.d(WifiDirectActivity.TAG, "Socket is opened");

            }else if(mode.equals(MODE_GROUP_OWNER)){
                Log.d(WifiDirectActivity.TAG, "Create socket : server mode");
                if(serverSocket == null){
                    try {
                        serverSocket = new ServerSocket();
                        serverSocket.setReuseAddress(true);
                        serverSocket.bind(new InetSocketAddress(8988));
                    } catch (IOException e) {
                        Log.e("ServerMode/Socket1", e.toString());
                    }
                }
                try {
                    socket = serverSocket.accept();
                } catch (IOException e) {
                    Log.e("ServerMode/Socket2", e.toString());
                }
                socketList.add(socket);
                Log.d(WifiDirectActivity.TAG, "Socket is opened");

            }
            if(socket == null)return null;
            try {
                InputStream inputStream = socket.getInputStream();
                BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));

                //メッセージを受け付ける処理
                String str;
                while ((str = reader.readLine()) != null){
                    Log.d(WifiDirectActivity.TAG, "Received : " + str);
                    publishProgress(str);
                }

                Log.d(WifiDirectActivity.TAG, "Reading finished");
            } catch (IOException e) {
                Log.e(WifiDirectActivity.TAG, e.toString());
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(String... values) {
            messageAdapter.add(values[0]);
        }
    }

    public void debug(String message){
        Button button = (Button) findViewById(R.id.button_connect);
        button.setText(message);
    }

}
