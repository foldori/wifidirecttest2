package com.example.kamiori.wifidirecttest2;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.NetworkInfo;
import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pDeviceList;
import android.net.wifi.p2p.WifiP2pInfo;
import android.net.wifi.p2p.WifiP2pManager;
import android.util.Log;

public class WifiDirectBroadcastReceiver extends BroadcastReceiver {
    private WifiP2pManager manager;
    private WifiP2pManager.Channel channel;
    private  WifiDirectActivity activity;

    public WifiDirectBroadcastReceiver(WifiP2pManager manager, WifiP2pManager.Channel channel, WifiDirectActivity activity) {
        super();

        this.manager = manager;
        this.channel = channel;
        this.activity = activity;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();

        if(WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION.equals(action)){
            //端末のWifiの有効/無効が変化したときに通知される

        }else if(WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION.equals(action)){
            //デバイス一覧に変化があったときに通知される
            if (manager != null) {
                Log.d(WifiDirectActivity.TAG, "Peers changed");

                //デバイス一覧を要求
                manager.requestPeers(channel, new WifiP2pManager.PeerListListener() {
                    @Override
                    //見つかったデバイス一覧
                    public void onPeersAvailable(WifiP2pDeviceList peers) {
                        activity.searchGroupOwner(peers);

                        activity.messageAdapter.clear();
                        for(WifiP2pDevice device : peers.getDeviceList()){
                            activity.messageAdapter.add(device.deviceName);
                        }
                        /*
                        if(mode.equals(WifiDirectActivity.MODE_GROUP_OWNER)){
                            activity.executeServer(peers);
                        }
                        */
                    }
                });
            }else{
                Log.d(WifiDirectActivity.TAG, "manager is null");
            }

        }else if(WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION.equals(action)){
            //自分の端末の通信状態に変化があったときに通知される
            Log.d(WifiDirectActivity.TAG, "State changed");
            if(manager != null){
                NetworkInfo networkInfo = (NetworkInfo) intent.getParcelableExtra(WifiP2pManager.EXTRA_NETWORK_INFO);
                if(networkInfo.isConnected()){
                    /*
                    //接続されたら、接続先の情報を取得する
                    //その後、クライアントモードの接続処理を行う
                    Log.d(WifiDirectActivity.TAG, "Connected");
                    activity.debug("Connected");

                    WifiP2pInfo info = (WifiP2pInfo) intent.getParcelableExtra(WifiP2pManager.EXTRA_WIFI_P2P_INFO);

                    if (mode.equals(WifiDirectActivity.MODE_CLIENT)) {
                        manager.requestConnectionInfo(channel, new WifiP2pManager.ConnectionInfoListener() {
                            @Override
                            public void onConnectionInfoAvailable(WifiP2pInfo info) {
                                activity.executeClient(info);
                            }
                        });
                    } else {
//                        activity.createServerSocket(info);
                    }
                    */
                    Log.d(WifiDirectActivity.TAG, "Connected");
                    WifiP2pInfo info = (WifiP2pInfo) intent.getParcelableExtra(WifiP2pManager.EXTRA_WIFI_P2P_INFO);
                    activity.setConnected(true);
                    activity.notifyConnection(info);
                }else{
                    Log.d(WifiDirectActivity.TAG, "not Connected");
                    //切断された
                    activity.setConnected(false);
                }
            }

        }else if(WifiP2pManager.WIFI_P2P_DISCOVERY_CHANGED_ACTION.equals(action)){
            //

        }else if(WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION.equals(action)){
            //自分の情報(INVITED, AVAILABLE等)が変化したときに通知される
        }
    }

}
